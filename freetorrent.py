#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 19 18:53:47 2018

@author: toitoinebzh
license : GPLv3

This software download every .torrent available on freetorrent.fr
Torrents are store in "TORRENT_LOCATION", you can then open them
with transmission and share free software.

enjoy ;)

"""

import re
import os
import urllib.request
from bs4 import BeautifulSoup


# some "constant"
WEBSITE = "http://freetorrent.fr/"


DIR_PATH = os.path.dirname(os.path.realpath(__file__))
TORRENT_LOCATION = DIR_PATH + "/FreetorrentFr/"

# classes


class Torrent:
    "class to represent torrents"

    def __init__(self, name, link):
        self.name = name
        self.link = link

    def get_link(self):
        "return torrent web page"
        return WEBSITE + self.link

    def download(self):
        "to download the torrent"
        html = urllib.request.urlopen(self.get_link())

        regex = "download\.php\?id=[1-9][0-9]{0,2}"
        soup = BeautifulSoup(html, "lxml", from_encoding='utf-8')

        text = re.search(regex, str(soup))
        file_id = text.group(0).split("=")[-1]
        file_link = WEBSITE + "download.php?id=" + file_id

        if not os.path.exists(TORRENT_LOCATION):
            os.makedirs(TORRENT_LOCATION)

        print("Downloading", self.name)
        urllib.request.urlretrieve(
            file_link,
            TORRENT_LOCATION +
            self.link +
            ".torrent")

    def __repr__(self):
        return self.name + " : " + self.get_link()


class FreetorrentPage:
    "class to download on freetorrent page"

    def __init__(self, nb):
        self.url = "http://freetorrent.fr/torrents.php?tri=postDate&ordre=desc&page=" + \
            str(nb)

    def get_torrents(self):
        "to get all the torrent on the page"
        html = urllib.request.urlopen(self.url)
        soup = BeautifulSoup(html, "lxml", from_encoding='utf-8')
        main_table = soup.find("table")

        torrents = []
        for line in main_table.find_all("tr")[1:]:
            name = line.find("a").text
            link = line.find("a").get("href")
            torrent = Torrent(name, link)
            torrents.append(torrent)

        return torrents

    def download(self):
        "to download all the torrent on the page"
        for torrent in self.get_torrents():
            torrent.download()

    def __repr__(self):
        return self.url + " : " + str(len(self.get_torrents())) + " torrents"


class Freetorrent:
    "class to downlaod all torrent on freetorrent.fr"

    def __init__(self, max_page=5):
        self.page = []
        self.max_page = max_page

    def get_pages(self):
        "to get all the pages"
        # max number of pages is 5 today !!
        for n_page in range(1, self.max_page + 1):
            fpage = FreetorrentPage(n_page)
            self.page.append(fpage)
        return self.page

    def get_torrents(self):
        "to get all the torrents"
        torrents = []
        for page in self.get_pages():
            torrents.extend(page.get_torrents())
        return torrents

    def download(self):
        "to download all the torrents"
        for torrent in self.get_torrents():
            torrent.download()

    def __repr__(self):
        return str(len(self.get_torrents())) + " torrents"


# =============================================================================
if __name__ == "__main__":
    print("This program download every torrent on freetorrent.fr")
    FT = Freetorrent()
    FT.download()
